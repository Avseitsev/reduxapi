import * as React from 'react';
import { NavLink, Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as NetflixStore from '../store/Netflix';


export class Layout extends React.Component<{}, {}> {
    public render() {
        return <div>
            <div>
                <div>
                    { this.props.children }
                </div>
            </div>
        </div>;
    }
}
