﻿import * as React from 'react';
import { Link, RouteComponentProps, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as NetflixStore from '../store/Netflix';
import axios from 'axios';
import { IMovies } from "../store/Netflix";
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Footer from './Footer';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import CircularProgress from 'material-ui/CircularProgress';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';


type NetflixProps =
    NetflixStore.NetflixState
    & typeof NetflixStore.actionCreators
    & RouteComponentProps<{}>;

export class OneView extends React.Component<NetflixProps, {}> {

    componentDidMount() {
    }


    public render() {
        return <MuiThemeProvider>           

                <div>
                    <NavLink exact to={'/'} activeClassName='active'>
                        <span className='glyphicon glyphicon-home'></span> НАЗАД
                </NavLink>

                    {this.props.movie != null &&
                    <div>
                        <div id="imgpic"> <img id="imgpic" style={{ width: "25%", height: 500, marginLeft: "5%", marginBottom: 30, marginTop: 30 }} src={"https://image.tmdb.org/t/p/w185_and_h278_bestv2" + this.props.movie.poster_path} /></div>
                        <Paper zDepth={2}>
                            <TextField hintText={"НАЗВАНИЕ: " + this.props.movie.title} style={{ marginLeft: 40, }} disabled underlineShow={false} />
                            <Divider />
                            <TextField hintText={"ПОПУЛЯРНОСТЬ : " + this.props.movie.popularity} style={{ marginLeft: 40, }} disabled underlineShow={false} />
                            <Divider />
                            <TextField hintText={"ПРОСМОТРЫ: " + this.props.movie.vote_count} style={{ marginLeft: 40, }} disabled underlineShow={false} />
                            <Divider />
                        </Paper>
                        <div style={{ color: "#fff", marginBottom: 10, marginTop: 10 }}> {this.props.movie.overview} </div>
                    </div>
                    }

                    {this.props.movie == null &&
                        <div>
                            <CircularProgress />
                        </div>
                    }

                    <Footer />
                </div>

        </MuiThemeProvider>;
    }
}

export default connect(
    (state: ApplicationState) => state.netflix,
    NetflixStore.actionCreators
)(OneView) as typeof OneView;
