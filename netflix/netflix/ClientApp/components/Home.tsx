﻿import * as React from 'react';
import { Link, RouteComponentProps, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as NetflixStore from '../store/Netflix';
import axios from 'axios';
import { IMovies } from "../store/Netflix";
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Footer from './Footer';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Serch from './Serch';

export const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
    },
    gridList: {
        width: "100%",
        height: 390,
        marginBottom: "5",
        display: 'flex',
        flexWrap: 'nowrap',
        overflowX: 'auto',
    },
    title: {
        cursor: 'pointer',
    },
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    },
};

type NetflixProps =
    NetflixStore.NetflixState
    & typeof NetflixStore.actionCreators
    & RouteComponentProps<{}>;

export class Movie extends React.Component<{ movie: IMovies, click:any}, {}>{    

    render() {
    return <NavLink exact to={'/View'} activeClassName='active' >
        <GridTile title={this.props.movie.title}
            onClick={this.props.click.bind(this, this.props.movie)}
            titleBackground="linear-gradient(to bottom, rgba(50,25,150,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
            actionIcon={<IconButton><StarBorder color="rgb(0, 133, 212)" /></IconButton>}
            subtitle={<span>POPULARITY: <b>{this.props.movie.popularity}</b>  <br></br> OVERVIEW: <b>{this.props.movie.overview}</b></span>}>
            <img src={"https://image.tmdb.org/t/p/w185_and_h278_bestv2" + this.props.movie.poster_path} className={"img"} />
         </GridTile>
    </NavLink>
    }
}


interface RegState {
    pageState: number;
    loadState: number;
}


export class Home extends React.Component<NetflixProps, RegState> {
    constructor() {
        super();
        this.state = { pageState: 1, loadState: 0};
    }


    componentDidMount() {
        this.netflixResponse();
    }

    netflixResponse() {
        var z = this;
        axios.get("https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=b732d95555e47a38bc32353240a46083")
            .then(function (response) {
                z.props.loadmovies(response.data.results);

            })
            .catch(function (error) {
                console.log(error);
            });

        axios.get("https://api.themoviedb.org/3/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=b732d95555e47a38bc32353240a46083")
            .then(function (response) {
                z.props.loadkmovies(response.data.results);
            })
            .catch(function (error) {
                console.log(error);
            });

        axios.get("https://api.themoviedb.org/3/discover/movie?with_genres=18&primary_release_year=2014&api_key=b732d95555e47a38bc32353240a46083")
            .then(function (response) {
                z.props.loadrmovies(response.data.results);
                z.setState({ loadState: 1 });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    out() {
       if (this.state.pageState == 0) {
           this.setState({ pageState: 1 });
       } else {
           this.setState({ pageState: 0 });
       }
    }

    log() {
        if (this.state.pageState != 2) {
            this.setState({ pageState: 2 });
        } else {
            this.setState({ pageState: 1 })
        }
    }


    public render() {
        return <MuiThemeProvider>
            <div>

            <Serch />

            {this.state.loadState != 1 &&
                    <RefreshIndicator
                        size={100}
                        left={650}
                        top={10}
                        loadingColor="#FF9800"
                        status="loading"
                        style={styles.refresh}
                    />
            }


            {this.state.loadState == 1 &&
            <div>
            <h3 className="h">ЖАНР ДНЯ : ЭКШН</h3>
                <GridList cellHeight={350} style={styles.gridList} cols={6} padding={13.50}  >
                        {this.props.Rmovies.map((value) => (
                            <Movie movie={value} click={this.props.loadmovie} />
                    ))}
                </GridList>

            <h3 className="h">ПОПУЛЯРНОЕ ДЕТСКОЕ</h3>
                <GridList cellHeight={350} style={styles.gridList} cols={6} padding={13.50}  >
                        {this.props.Kmovies.map((value) => (
                            <Movie movie={value} click={this.props.loadmovie} />
                    ))}
                </GridList>

            <h3 className="h">ПОПУЛЯРНО СЕЙЧАС</h3>
                <GridList cellHeight={350} style={styles.gridList} cols={6} padding={13.50}  >
                        {this.props.movies.map((value) => (
                            <Movie movie={value} click={this.props.loadmovie} />
                ))}
            </GridList>
             </div>
            }

                <div className="menu">
                    <div className="panel" >
                        <a className="handle" href="#"></a>
                        <div className="form">
                            {this.state.pageState ==0 &&
                                <div className="register-form">
                                    <input type="text" placeholder="имя" />
                                    <input type="password" placeholder="пароль" />
                                    <input type="password" placeholder="повтор пароля" />
                                    <button >создать</button>
                                    <p className="message" >Уже есть аккаунт?? <a onClick={this.out.bind(this)} >Войти</a></p>
                                </div>
                            }
                            {this.state.pageState == 1 &&
                                <div className="login-form">
                                    <input type="text" placeholder="имя" />
                                    <input type="password" placeholder="пароль" />
                                    <button onClick={this.log.bind(this)}>войти</button>
                                    <p className="message" >Нет аккаунта? <a onClick={this.out.bind(this)} >Создать аккаунт</a></p>
                                </div>
                            }
                            {this.state.pageState == 2 &&
                                <div className="login-form">
                                <button onClick={this.log.bind(this)}>выйти</button>
                                <p className="message" ></p>
                                <p className="message" >Уже уходишь??!</p>
                                <p className="message" ></p>
                                </div>
                            }
                        </div>
                    </div>
                </div>

                <Footer />
            </div>
            </MuiThemeProvider>;
    }
}

export default connect(
    (state: ApplicationState) => state.netflix, 
    NetflixStore.actionCreators                
)(Home) as typeof Home;
