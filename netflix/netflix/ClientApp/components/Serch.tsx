﻿import * as React from 'react';
import * as NetflixStore from '../store/Netflix';
import { NavLink, Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { IMovies } from "../store/Netflix";
import { ApplicationState } from '../store';
import axios from 'axios';
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import { Movie, styles} from "./Home";
import debounce from 'lodash.debounce';
import List from '../custom/List';


type NetflixProps =
    NetflixStore.NetflixState
    & typeof NetflixStore.actionCreators
    & RouteComponentProps<{}>;



export class Serch extends React.Component<any, {}> {

    serch() {
        var z = this;
        if ((document.getElementById("serch") as HTMLInputElement).value.length > 1) {
            axios.get("https://api.themoviedb.org/3/search/multi?api_key=b732d95555e47a38bc32353240a46083&language=en-US&query=" + (document.getElementById("serch") as HTMLInputElement).value + "&page=1&include_adult=false")
                .then(function (response) {
                    z.props.serchmovies(response.data.results);
                })
                .catch(function (error) {
                    z.props.serchmovies([]);
                    console.log(error);
                });
        } else {
            z.props.serchmovies([]);
        }
    }

    am(text: string) {
        (document.getElementById("serch") as HTMLInputElement).value = text;
        this.serch();
    }


    public render() {
        return <div>
        <div className="d1">
                <div>
                    <input id="serch" type="text" placeholder="Искать здесь..." onChange={debounce(this.serch.bind(this), 1000)} /> <List list={["avatar", "фильм", "fury", "фильм1", "фильм2", "фильм3", "фильм1", "фильм2", "фильм3"]} title="ЧАСТЫЕ ЗАПРОСЫ" click={this.am} context={this}/>
            </div>
        </div>
        {this.props.serch != "" &&
            <GridList cellHeight={350} style={styles.gridList} cols={6} padding={13.50}  >
                {this.props.serch.map((value) => (
                    <Movie movie={value} click={this.props.loadmovie}/>
                ))}
                </GridList>
        }
        </div>;

    }
}



export default connect(
    (state: ApplicationState) => state.netflix,
    NetflixStore.actionCreators
)(Serch) as typeof Serch;

