﻿import * as React from 'react';
import { NavLink, Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';

export default class Footer extends React.Component<any, {}> {
    public render() {
        return<footer>
            <ul>
                <li>
                    <p className="home">Home</p>
                    <a className="logo" href="#">movies bottle<i>&copy; 2013</i></a>
                </li>
                <li>
                    <p className="services">Services</p>

                    <ul>
                        <li><a href="#">3D modeling</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Mobile version</a></li>
                        <li><a href="#">Web &amp; Print Design</a></li>
                    </ul>
                </li>
                <li>
                    <p className="reachus">Reach us</p>

                    <ul>
                        <li><a href="#">Email</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                        <li>555-123456789</li>
                    </ul>
                </li>
                <li>
                    <p className="clients">Clients</p>

                    <ul>
                        <li><a href="#">Login Area</a></li>
                        <li><a href="#">Support Center</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </li>
            </ul>
        </footer>

    }
}