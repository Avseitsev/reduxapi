﻿import * as React from 'react';
import * as NetflixStore from '../store/Netflix';
import { NavLink, Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { IMovies } from "../store/Netflix";
import { ApplicationState } from '../store';
import axios from 'axios';
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import { Movie } from "../components/Home";
import debounce from 'lodash.debounce';

interface ListState {
    buttonState: boolean;
}


export default class List extends React.Component<{ list: Array<string>, title: string, click: any /*функция, прелетает снизу, на вход должен идти содержимое элементы вхыод что хош*/, context: any/*передаю контекст что бы вызывать функцию на стороне контекста откуда пришло, ибо тогда если в функции есть функция я ее не смогу вызвать*/}, ListState> {
    constructor() {
        super();
        this.state = { buttonState: false };
    }

    public render() {
        return <div>

            {this.state.buttonState == false &&
                <button onClick={() => this.setState({ buttonState: true })}>{this.props.title}</button>
            }
            {this.state.buttonState == true &&
            <div>
                <button style={{ color: "red" }} onClick={() => this.setState({ buttonState: false })}>X</button>
                <div>
                    <div className="brd" style={{ width: "20%" }}>{this.props.title}</div>
                    <div className="container2" style={{ width: "20%" }}>
                        <div className="box">
                            {this.props.list.map((value) => (
                                <div onClick={this.props.click.bind(this.props.context, value)}>{value}</div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            }


        </div>;
    }

}