﻿import { Action, Reducer } from 'redux';

export interface NetflixState {
    movies: Array<IMovies>;
    Rmovies: Array<IMovies>;
    Kmovies: Array<IMovies>;
    serch: Array<IMovies>;
    movie: IMovies;
}

export interface IMovies {
    vote_count: string;
    id: number;
    video: boolean;
    vote_average: string;
    title: string;
    popularity: string;
    backdrop_path: string;
    overview: string;
    poster_path: string;
}

interface LoadMovie {
    type: 'LOADMOVIE'
    payload: IMovies
}
interface LoadMovies {
    type: 'LOADMOVIES'
    payload: Array<IMovies>
}
interface LoadRMovies {
    type: 'LOADRMOVIES'
    payload: Array<IMovies>
}
interface LoadKMovies {
    type: 'LOADKMOVIES'
    payload: Array<IMovies>
}
interface SerchMovies {
    type: 'SERCHMOVIES'
    payload: Array<IMovies>
}
interface DecrementCountAction { type: 'ERROR' }

type KnownAction = DecrementCountAction | LoadMovies | LoadRMovies | LoadKMovies | SerchMovies | LoadMovie;


export const actionCreators = {
    error: () => <DecrementCountAction>{ type: 'ERROR' },
    loadmovies: (moves: Array<IMovies>) => <LoadMovies>{ type: 'LOADMOVIES', payload: moves },
    loadrmovies: (moves: Array<IMovies>) => <LoadRMovies>{ type: 'LOADRMOVIES', payload: moves },
    loadkmovies: (moves: Array<IMovies>) => <LoadKMovies>{ type: 'LOADKMOVIES', payload: moves },
    serchmovies: (moves: Array<IMovies>) => <SerchMovies>{ type: 'SERCHMOVIES', payload: moves },
    loadmovie: (moves: IMovies) => <LoadMovie>{ type: 'LOADMOVIE', payload: moves },

};


export const reducer: Reducer<NetflixState> = (state: NetflixState, action: KnownAction) => {
    switch (action.type) {
        case 'ERROR':
            return state;
        case 'LOADMOVIES': {
            return { movies: action.payload, Rmovies: state.Rmovies, Kmovies: state.Kmovies, serch: state.serch, movie: state.movie};
        }
        case 'LOADRMOVIES': {
            return { movies: state.movies, Rmovies: action.payload, Kmovies: state.Kmovies, serch: state.serch, movie: state.movie };
        }
        case 'LOADKMOVIES': {
            return { movies: state.movies, Rmovies: state.Rmovies, Kmovies: action.payload, serch: state.serch, movie: state.movie };
        }
        case 'SERCHMOVIES' : {
            return { movies: state.movies, Rmovies: state.Rmovies, Kmovies: state.Kmovies, serch: action.payload, movie: state.movie };
        }
        case 'LOADMOVIE': {
            return { movies: state.movies, Rmovies: state.Rmovies, Kmovies: state.Kmovies, serch: state.serch, movie: action.payload };
        }
        default:
            const exhaustiveCheck: never = action;
    }

    return state || { movies: [], Rmovies: [], Kmovies: [], serch: [], movie:null };
};
