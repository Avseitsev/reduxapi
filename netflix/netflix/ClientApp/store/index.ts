import * as Netflix from './Netflix';

export interface ApplicationState {
    netflix: Netflix.NetflixState;
}

export const reducers = {
    netflix: Netflix.reducer
};

export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
